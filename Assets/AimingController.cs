using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using StarterAssets;
using UnityEngine.InputSystem;
using UnityEngine.Animations.Rigging;

public class AimingController : MonoBehaviour
{
	[SerializeField]
	private Rig AimRig;
    [SerializeField]
    private CinemachineVirtualCamera AimCam;
	[SerializeField]
	private LayerMask aimColliderMask = new LayerMask();
	[SerializeField]
	private Transform DebugActor;

	// Bullet Type
	[SerializeField]
	GameObject BulletType;
	[SerializeField]
	GameObject MuzzlePoint;
	ThirdPersonController TPC;

	private StarterAssetsInputs SAI;

	private void Awake()
	{
		SAI = GetComponent<StarterAssetsInputs>();
		TPC = GetComponent<ThirdPersonController>();
	}
	private void Update()
	{
		Vector3 mouseWorldPosition = Vector3.zero;

		Vector2 ScreenCenterPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
		Ray ray = Camera.main.ScreenPointToRay(ScreenCenterPoint);
		if (Physics.Raycast(ray,out RaycastHit rch,999f, aimColliderMask))
		{
			DebugActor.position = rch.point;
			mouseWorldPosition = rch.point;
		}
		else
		{
			DebugActor.position = transform.position + ray.direction * 999f;
			mouseWorldPosition = transform.position + ray.direction * 999f;
		}

		if (SAI.aim)
		{
			AimCam.gameObject.SetActive(true);
			TPC.SetRotateOnMove(false);
		}
		else
		{
			AimCam.gameObject.SetActive(false);
			TPC.SetRotateOnMove(true);
		}
		Vector3 WorldAimTarget = mouseWorldPosition;
		WorldAimTarget.y = transform.position.y;
		Vector3 aimDirection = (WorldAimTarget - transform.position).normalized;
		transform.forward = Vector3.Lerp(transform.forward, aimDirection, Time.deltaTime * 20f);

		if (SAI.shoot)
		{
			Vector3 aimDir = (mouseWorldPosition - MuzzlePoint.transform.position).normalized;
			Quaternion Dir = Quaternion.LookRotation(aimDir,Vector3.up);
			Instantiate(BulletType, MuzzlePoint.transform.position, Dir);
			SAI.shoot = false;
		}
	}
}
