using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    [SerializeField]
    float BulletSpeed = 50;
    // Start is called before the first frame update
    void Start()
    {
        Rigidbody rb =GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * BulletSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag=="Ball")
		{
            collision.gameObject.GetComponent<BallScript>().CheckBall();
        }
        Destroy(gameObject);
	}
}
