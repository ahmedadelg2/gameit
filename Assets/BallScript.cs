using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour
{
    public int BallTurn=0;
    public BallSpawner BallSpawnerRef;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeMaterial(Material mat)
	{
        GetComponent<MeshRenderer>().material = mat;
	}

    public void CheckBall()
	{
        Debug.Log(BallTurn.ToString() + " /// " + BallSpawnerRef.CurrentTurn.ToString());
        if (BallTurn == BallSpawnerRef.CurrentTurn)
		{
            BallSpawnerRef.CurrentTurn++;
            Destroy(gameObject);
		}
	}
}
